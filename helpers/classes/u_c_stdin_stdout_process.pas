unit u_c_stdin_stdout_process;
  interface
    uses Classes // , Dialogs
        , Windows // , Forms, Messages
        , u_environment
        , u_c_basic_object
        ;

    type c_stdin_stdout_process= class; // forward
         t_po_stdin_stdout_process_event= procedure (p_c_stdin_stdout_process: c_stdin_stdout_process) of Object ;

         t_pipe= record
                   m_read_handle, m_write_handle: tHandle;
                 end;

         t_window_style= (wsNormal, wsMinimize, wsMaximize, wsHide, wsMinNoActivate,
             wsShowNoActivate);
         t_priority_type= (ptNormal, ptIdle, ptHigh, ptRealTime);

         c_stdin_stdout_process= class(c_basic_object)
                                   m_priority_type: t_priority_type;
                                   m_window_style: t_window_style;
                                   m_exe_name: String;
                                   m_exe_parameters: String;
                                   m_directory: String;
                                   m_c_environment_list: tStringList;

                                   // -- wants the message
                                   m_do_call_process_messages: boolean;

                                   m_process_handle: Cardinal;
                                   m_stdin_pipe, m_stdout_pipe: t_pipe;

                                   m_reception_buffer: array[0..1023] of char;
                                   m_c_environment: c_environment;
                                   // -- can concatenate several read
                                   m_received_string: String;

                                   m_last_error: DWORD;
                                   m_error_text: String;
                                   m_exit_code: DWORD;

                                   m_on_output: t_po_stdin_stdout_process_event;
                                   m_on_error: t_po_stdin_stdout_process_event;

                                   constructor create_stdin_stdout_process(p_name: String);

                                   procedure handle_error(p_text: String);
                                   function f_is_running: boolean;

                                   function f_create_stdin_stdout_pipes: Boolean;
                                   procedure close_stdin_stdout_pipes;
                                   function f_create_process: Boolean;

                                   procedure write_string(p_string: AnsiString);
                                   procedure handle_received(p_received_count: Integer);
                                   procedure read_string;

                                   procedure do_stop_process;

                                   destructor Destroy; override;
                                 end; // c_stdin_stdout_process

    function f_create_pipe(Var pv_pipe: t_pipe): boolean;

  implementation
    uses SysUtils, Forms, u_types_constants, u_c_display, u_display_hex_2;

    function f_create_pipe(Var pv_pipe: t_pipe): boolean;
      const k_pipe_buffer_size= 4096;
      begin
        with pv_pipe do
        begin
          // -- Create the pipe
          result:= CreatePipe(m_read_handle, m_write_handle, nil, k_pipe_buffer_size);

          // -- recreate the handles, this time with the "inheritable" flag
          if result
            then result:= DuplicateHandle(GetCurrentProcess, m_read_handle,
                GetCurrentProcess, @ m_read_handle, 0, True,
                DUPLICATE_CLOSE_SOURCE OR DUPLICATE_SAME_ACCESS);

          if result
            then result:= DuplicateHandle(GetCurrentProcess, m_write_handle,
                GetCurrentProcess, @ m_write_handle, 0, True,
                DUPLICATE_CLOSE_SOURCE OR DUPLICATE_SAME_ACCESS);
        end;
      end; // f_create_pipe

    procedure close_pipe(p_pipe: t_pipe);
      begin
        with p_pipe do
        begin
          CloseHandle(m_read_handle);
          CloseHandle(m_write_handle);
        end;
      end; // close_pipe

    procedure SendDebug(s: string);
      begin
        MessageBox(0, pointer(s), 'DEBUG', MB_OK);
      end; // SendDebug

    // -- c_stdin_stdout_process

    constructor c_stdin_stdout_process.create_stdin_stdout_process(p_name: String);
      begin
        inherited create_basic_object(p_name); // Create(AOwner);

        m_window_style:= wsHide;
      end; // Create

    procedure c_stdin_stdout_process.handle_error(p_text: String);
      begin
        m_last_error:= GetLastError;
        m_error_text:= p_text+ ' '+ SysErrorMessage(GetLastError);
        if Assigned(m_on_error)
          then m_on_error(Self);
      end; // handle_error

    function c_stdin_stdout_process.f_is_running: boolean;
      begin
        Result:= WaitForSingleObject(m_process_handle, 0)<> WAIT_OBJECT_0;
      end; // f_is_running

    function c_stdin_stdout_process.f_create_stdin_stdout_pipes: Boolean;
      begin
        Result:= f_create_pipe(m_stdin_pipe)
            and f_create_pipe(m_stdout_pipe)
      end; // f_create_stdin_stdout_pipes

    procedure c_stdin_stdout_process.close_stdin_stdout_pipes;
      begin
        close_pipe(m_stdin_pipe);
        close_pipe(m_stdout_pipe);
      end; // close_stdin_stdout_pipes

    function c_stdin_stdout_process.f_create_process: Boolean;
      const k_process_creation_flags: DWord= NORMAL_PRIORITY_CLASS;
      var l_window_style: dWord;
          l_startup_info: TStartupInfo;
          l_pt_directory_z: pChar;
          l_process_information: PROCESS_INFORMATION;
          l_pt_environment: Pointer;
      begin
        display('> f_create_process');

        Case m_window_style of
          wsNormal: l_window_style:= SW_SHOWNORMAL;
          wsMinimize: l_window_style:= SW_SHOWMINIMIZED;
          wsMaximize: l_window_style:= SW_SHOWMAXIMIZED;
          wsHide: l_window_style:= SW_HIDE;
          wsMinNoActivate: l_window_style:= SW_SHOWMINNOACTIVE;
          wsShowNoActivate: l_window_style:= SW_SHOWNA;
          else
             l_window_style:= SW_HIDE;
             display_bug_stop('unknown style');
        end; // case m_window_style

        // -- initialize the StartupInfo
        Zeromemory(@ l_startup_info, SizeOf(l_startup_info));
        with l_startup_info do
        begin
          cb:= sizeof(l_startup_info); // Size of the structure. The OS uses that to validat it
          dwFlags:= STARTF_USESHOWWINDOW or // Use the wShowWindow field
              STARTF_USESTDHANDLES; // use the handles we created for the std IO pipes
          wShowWindow:= l_window_style; // Show the console or not...
          hStdInput:= m_stdin_pipe.m_read_handle; // StdIO.stdIn.hRead; // read input from input pipe
          hStdError:= m_stdout_pipe.m_write_handle; // StdIO.stdError.hWrite; // Write errors to the error pipe
          hStdOutput:= m_stdout_pipe.m_write_handle; // StdIO.stdOut.hWrite; // Write Ouput to output pipe
        end; // with l_startup_info

        if m_directory= ''
          then l_pt_directory_z:= nil
          else l_pt_directory_z:= PChar(m_directory+ m_exe_parameters);

        display('> create_environment');
        if m_c_environment_list= Nil
          then l_pt_environment:= Nil
          else begin
              m_c_environment:= c_environment.create_environment(m_c_environment_list);
              l_pt_environment:= @ m_c_environment.m_oa_environment[0];
            end;
        display('< create_environment');

        // -- spawn the child process
        // -- CreateProcess(lpApplicationName: PChar; lpCommandLine: PChar;
        // --   lpProcessAttributes: PSecurityAttributes; lpThreadAttributes: PSecurityAttributes;
        // --   bInheritHandles: BOOL
        // --   param or priority;
        // --   environment, pt_dir_z
        if CreateProcess(nil, // must be NIL for win16 apps under NT, including DOS progs
            PChar(m_exe_name),
            nil, // Process security attibutes. Nil= same as current
            nil, // Thread security attibutes. Nil= same as current
            True, // Inherite handles. MUST be true for IO redirection
            k_process_creation_flags, // creation flags
            l_pt_environment, // If CGI aplication, used to pass the HTTP headers
            l_pt_directory_z, // Directory of the new process
            l_startup_info, // startupinfo used to initialize the new process
            l_process_information) // returned structure used thereafter to control the newly created process
          then begin
              Result:= True;
              m_process_handle:= l_process_information.hProcess;
            end
          else begin
              handle_error('CreateProcess');
              RaiseLastOSError;
              close_stdin_stdout_pipes;
              Result:= False;
            end;

        display('< f_create_process');
      end; // f_create_process

    procedure c_stdin_stdout_process.write_string(p_string: AnsiString);
      var l_bytes_written_count: Cardinal;
      begin
        if p_string= ''
          then Exit;

        if WriteFile(m_stdin_pipe.m_write_handle, p_string[1], Length(p_string),
            l_bytes_written_count, nil)
          then begin
              display('> wrote '+ f_integer_to_hex(l_bytes_written_count));
              Application.ProcessMessages;
              display('< write_string');
            end
          else handle_error(SysErrorMessage(GetLastError));
      end; // write_string

    procedure c_stdin_stdout_process.handle_received(p_received_count: Integer);
      var l_string: String;
      begin
        display('> hanle_received');

        SetLength(l_string, p_received_count);
        Move(m_reception_buffer, l_string[1], p_received_count);

        // -- concatenate
        m_received_string:= m_received_string+ l_string;
        
        if assigned(m_on_output)
          then m_on_output(self);
        if m_do_call_process_messages
          then Forms.Application.ProcessMessages;
        display('< hanle_received');
      end; // handle_received

    procedure c_stdin_stdout_process.read_string;
      var l_process_exit_code: Cardinal;

          l_bytes_to_read_count, l_bytes_read_count: Cardinal;
          l_bytes_read_remaining_count: Cardinal;
      begin
        display('> read_string');

        // -- check if the process is still active
        GetExitCodeProcess(m_process_handle, l_process_exit_code);
        if l_process_exit_code<> STILL_ACTIVE
          then begin
              display('no_longer_active');
            end
          else begin
              // -- wait until the exe has sent something
              display('> WaitForSingleObject');
              while WaitForSingleObject(m_process_handle, 0)<> WAIT_OBJECT_0 do;
              display('< WaitForSingleObject');
            end;

        if m_stdout_pipe.m_read_handle= INVALID_HANDLE_VALUE
          then display_bug_stop('invalid handle');

        // -- check to see if there is any data to read from stdout
        PeekNamedPipe(m_stdout_pipe.m_read_handle, @ m_reception_buffer, 1024,
            @ l_bytes_to_read_count, @ l_bytes_read_count, nil);

        display('to_read '+ IntToStr(l_bytes_to_read_count)+ ' available '+ IntToStr(l_bytes_read_count));
        if l_bytes_read_count<> 0
          then begin
              FillChar(m_reception_buffer, sizeof(m_reception_buffer), 'z');

              // -- read by 1024 bytes chunks
              l_bytes_read_remaining_count:= l_bytes_read_count;
              while l_bytes_read_remaining_count>= 1023 do
              begin
                // -- read the stdout pipe
                ReadFile(m_stdout_pipe.m_read_handle, m_reception_buffer, 1024, l_bytes_read_count, nil);
                Dec(l_bytes_read_remaining_count, l_bytes_read_count);
                handle_received(l_bytes_read_count);
              end;

              if l_bytes_read_remaining_count> 0
                then begin
                    ReadFile(m_stdout_pipe.m_read_handle, m_reception_buffer,
                        l_bytes_read_remaining_count, l_bytes_read_count, nil);
                    display('read '+ IntToStr(l_bytes_read_count));
                    handle_received(l_bytes_read_count);
                  end;
            end; // received some bytes

        display('< read_string');
      end; // read_string

    procedure c_stdin_stdout_process.do_stop_process;
        // -- the client requests a stop
      begin
        display('> do_stop_process');

        GetExitCodeProcess(m_process_handle, m_exit_code);
        if m_exit_code= STILL_ACTIVE
          then begin
              display('terminate');
              if not TerminateProcess(m_process_handle, m_exit_code)
                then handle_error('terminate_pb');
            end;

        display('< do_stop_process');
      end; // do_stop_process

    destructor c_stdin_stdout_process.Destroy;
      begin
        close_stdin_stdout_pipes;
        m_c_environment.Free;

        inherited;
      end; // Destroy

    begin // u_c_yyy
    end.// u_c_yyy




