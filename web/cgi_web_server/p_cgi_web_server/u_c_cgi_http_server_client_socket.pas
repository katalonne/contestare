unit u_c_cgi_http_server_client_socket;
  interface
    uses u_c_basic_object
        , u_c_server_socket_4
        , u_c_http_server_client_socket
        ;

    type c_cgi_http_server_client_socket= Class; // Forward
         t_po_cgi_http_server_client_socket_event= Procedure(p_c_cgi_http_server_client_socket: c_cgi_http_server_client_socket) of Object;

         c_cgi_http_server_client_socket=
             class(c_http_server_client_socket)
               m_cgi_exe_path: String;
               m_exe_name: String;
               m_show_console: Boolean;
               m_post_content: String;

               m_on_cgi_http_server_client_socket_closed: t_po_cgi_http_server_client_socket_event;

               Constructor create_server_client_socket(p_name: String;
                   p_c_server_socket_ref: c_server_socket); Override;

               procedure handle_received_data; Override;
               procedure send_and_receive_cgi;

               Destructor Destroy; Override;
             end; // c_cgi_http_server_client_socket

  implementation
    uses Windows, Classes, SysUtils
        , u_c_display, u_types_constants, u_characters, u_strings
        , u_file
        , u_c_stdin_stdout_process
        ;

    // -- c_cgi_http_server_client_socket

    Constructor c_cgi_http_server_client_socket.create_server_client_socket(p_name: String;
        p_c_server_socket_ref: c_server_socket);
      begin
        Inherited create_server_client_socket(p_name, p_c_server_socket_ref);
      end; // create_server_client_socke

    procedure c_cgi_http_server_client_socket.handle_received_data;
        // -- take the header appart
        // -- should at least retrieve
        // --  - the method GET
        // --  - the page path and file name
      var l_end_of_header_position: Integer;
          l_read_index: Integer;
      begin
        trace_socket('> c_cgi_client.read ');
        // display(m_c_reception_buffer.f_display_read_write_index);

        // -- get the data and handle GET if any
        Inherited handle_received_data;
        // display(m_c_reception_buffer.f_display_read_write_index);

        if m_page_name= ''
          then
            with m_c_reception_buffer do
            begin
              display('> analyze_header');

              l_read_index:= f_string_position(m_read_index, 'post');
              if l_read_index< 0
                then l_read_index:= f_string_position(0, 'POST');

              if m_read_index< 0
                then m_http_answer_error:= e_no_GET
                else begin
                    display('POST');
                    // -- |HREF="http://www.felix-colibri.com/scripts/coliget.exe?a=1?"interbase">
                    // -- => receives
                    // --   |POST /scripts/coliget.exe HTTP/1.1 RET
                    // --   | other header stuff
                    // --   |interbase

                    // -- skip POST
                    m_read_index:= l_read_index+ 4;
                    // -- skip blanks
                    skip_blanks(m_read_index);
                    // -- extract the cgi name
                    m_exe_name:= f_extract_non_blank(m_read_index);

                    display('script_exe >'+ m_exe_name+ '<');

                    // -- should:
                    // --  extract all the header items (MIME etc)
                    // --  find the Content-Length
                    // --  fetch content until read all Content-Length => start cgi_exe

                    // -- fetch the content
                    l_end_of_header_position:= f_next_2_new_line_position(m_read_index);
                    if l_end_of_header_position> 0
                      then begin
                          m_post_content:= f_extract_string_start_end(l_end_of_header_position+ 4, m_write_index- 1);
                          display('content '+ m_post_content+ '|');
                        end;

                    send_and_receive_cgi;

                    // -- cleanup buffer
                    m_c_reception_buffer.clear_buffer;

                    display('close_win_socket');

                    do_close_win_socket;
                    if Assigned(m_on_cgi_http_server_client_socket_closed)
                      then m_on_cgi_http_server_client_socket_closed(Self);

                    // Free;
                  end; // handle POST
              display('< analyze_header');
            end; // with p_c_byte_buffer

        trace_socket('< c_cgi_client.read');
      end; // handle_server_client_socket_received_data

    procedure c_cgi_http_server_client_socket.send_and_receive_cgi;
        // -- received POST, cgi_exe, parameters, and FORM result
        // --   |POST /scripts/coliget.exe HTTP/1.1 RET
      var l_cgi_path, l_cgi_exe_file_name: String;

      function f_found_cgi_exe: boolean;
        var l_cgi_exe_full_file_name: String;
        begin
          display('|'+ m_exe_name+ '|');
          l_cgi_exe_full_file_name:= f_replace_character(m_exe_name, '/', '\');
          // -- change into an absolute path
          Delete(l_cgi_exe_full_file_name, 1, Length('\scripts\'));
          display(l_cgi_exe_full_file_name);

          Result:= FileExists(l_cgi_exe_full_file_name);
          if Result
            then begin
                display('Ok');
                l_cgi_path:= ExtractFilePath(l_cgi_exe_full_file_name);
                l_cgi_exe_file_name:= ExtractFileName(l_cgi_exe_full_file_name);
              end;
        end; // f_found_cgi_exe

      procedure launch_cgi;

        procedure send_answer_to_client(p_content: String);
          var l_header: String;
              // l_mime_type, 
          begin
            // -- the cgi.exe has built
            // --   |Content_type=xxx
            // --   |Content-Length=xxx
            // --   |
            // --   |yyyy
            // -- prepend the status
            // l_MIME_type:= f_mime_type(m_page_name);

            l_header:=
                  k_answer_http_200_ok
                + k_answer_server
                // + f_answer_content_type(l_MIME_type)
                // + f_answer_content_length(Length(p_content))
                // + k_new_line;
                ;
            f_do_send_string(l_header+ p_content);
          end; // send_answer_to_client

        begin // send_and_receive_cgi
          // -- write the FORM results
          // -- to read and write, use a process

          with c_stdin_stdout_process.create_stdin_stdout_process('runner') do
          begin
            m_exe_name:= l_cgi_path+ l_cgi_exe_file_name;

            // -- initialize the process parameters
            if m_show_console
              then m_window_style:= wsNormal
              else m_window_style:= wsHide;
            m_do_call_process_messages:= True;
            m_c_environment_list:= tStringList.Create;
            with m_c_environment_list do
            begin
              Add('CONTENT_LENGTH='+ IntToStr(Length(m_post_content)));
              Add('REQUEST_METHOD=POST');
              Add('REMOTE_ADDR=127.0.0.1');
            end;

            if f_create_stdin_stdout_pipes
              then begin
                  display('pipe_ok');
                  if f_create_process
                    then begin
                        display('process_ok');

                        write_string(m_post_content);

                        read_string;
                        // -- debug
                        display(f_display_string(m_received_string));

                        // -- send this back to the client
                        send_answer_to_client(m_received_string);
                      end
                    else display('process NOK');
                end
              else display('pipe NOK')  ;

            Free;
          end; // with c_stdin_stdout_process
        end; // launch_cgi

      begin // send_and_receive_cgi
        if f_found_cgi_exe
          then launch_cgi
          else send_page_not_found(m_exe_name);
      end; // send_and_receive_cgi

    Destructor c_cgi_http_server_client_socket.Destroy;
      begin
        Inherited;
      end; // Destroy

    begin // u_c_cgi_http_server_client_socket
    end. // u_c_cgi_http_server_client_socket
