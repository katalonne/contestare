unit u_environment;
  interface
    uses Classes;

    function f_write_environment_key_value(p_key, p_value: String): Boolean;
    function f_get_environment_value(p_key: pChar): String;
    function f_get_all_environment_string: String;

    type c_environment= Class
                          m_size: Integer;
                          m_oa_environment: array of char;
                          constructor create_environment(p_c_strings: tStrings);
                        end; // c_environment

  implementation
    uses Windows
        , SysUtils // StrPas
        ;

    function f_write_environment_key_value(p_key, p_value: String): Boolean;
      begin
        if (p_key<> '') and (p_value<> '')
          then Result:= SetEnvironmentVariable(pChar(p_key), pChar(p_value));
      end; // write_environment_key_value

    function f_get_environment_value(p_key: pChar): String;
      const k_max= 255;
      var l_environment_value: String[k_max];
          l_size: Integer;
      begin
        l_size:= GetEnvironmentVariable(p_key, @ l_environment_value[1], k_max);
        l_environment_value[0]:= chr(l_size);
        Result:= l_environment_value;
      end; // f_get_environment_value

    function f_get_all_environment_string: String;
        // -- get all the environment variables
      var l_pchar: PChar;
          l_running_pchar: pChar;
          l_environment_line: ShortString;
      begin
        l_pchar:= GetEnvironmentStrings;

        Result:= '';
        l_running_pchar:= l_pchar;

        while l_running_pchar^<> #0 do
        begin
          l_environment_line:= StrPas(l_running_pchar);
          Result:= Result+ l_environment_line+ chr(13)+chr(10);
          Inc(l_running_pchar, StrLen(l_running_pchar)+ 1)
        end; // while

        FreeEnvironmentStrings(l_pchar);
      end; // f_get_all_environment_string

    // -- c_environment

    constructor c_environment.create_environment(p_c_strings: tStrings);
        // -- create a block of xxx0yyy0zzz00
      var l_line: Integer;
          l_position: Integer;
      begin
        m_size:= 0;
        if p_c_strings<> Nil
          then
            with p_c_strings do
            begin
              for l_line:= 0 to Count- 1 do
                Inc(m_size, Length(Strings[l_line])+ 1);
              Inc(m_size);

              SetLength(m_oa_environment, m_size);
              l_position:= 0;
              for l_line:= 0 to Count- 1 do
              begin
                System.Move(p_c_strings[l_line][1], m_oa_environment[l_position], Length(Strings[l_line]));
                Inc(l_position, Length(Strings[l_line]));
                m_oa_environment[l_position]:= Chr(0);
                Inc(l_position);
              end;
              m_oa_environment[m_size- 1]:= Chr(0);
            end;
      end; // c_environment

    begin // u_c_yyy
    end. // u_c_yyy
