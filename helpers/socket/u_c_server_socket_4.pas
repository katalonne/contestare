unit u_c_server_socket_4;
  interface
    uses Classes, Windows, Messages, WinSock
      , SysUtils // exception
      , u_c_basic_object
      , u_c_base_socket_4
      , u_c_base_socket_with_buffer_4
      ;

    const wm_asynch_select= wm_User;

          k_default_listen_queue_size= 5;

    type c_server_client_socket= Class;
         // -- triggered when received data
         t_po_server_client_socket_event= Procedure(p_c_server_client_socket: c_server_client_socket) of Object;
         // -- this CLASS reference will allow the creation of c_server_client_socket descendents
         c_class_of_server_client_socket= Class of c_server_client_socket;

         c_server_socket= Class; // forward

         c_server_client_socket= class(c_base_socket_with_buffer)
                                   // -- back link to the c_server_socket
                                   m_c_server_socket_ref: c_server_socket;

                                   // -- VIRTUAL: create different descendents
                                   Constructor create_server_client_socket(p_name: String;
                                       p_c_server_socket_ref: c_server_socket); Virtual;

                                   // -- Empty handlers. Will be Overrided by descendents
                                   procedure handle_can_write_data; Virtual;
                                   procedure handle_received_data; Virtual;
                                   procedure handle_remote_client_closed; Virtual;

                                   function f_display_server_client_socket: String;
                                 end; // c_server_client_socket

         c_server_client_socket_list= Class(c_base_socket)
                                        m_c_server_client_socket_list: tStringList;

                                        constructor create_server_client_socket_list(p_name: String);

                                        function f_c_server_client_socket(p_index: Integer): c_server_client_socket;
                                        procedure display_server_client_socket_list;
                                        procedure add_server_client_socket(p_socket_handle_string: String;
                                            p_c_server_client_socket: c_server_client_socket);
                                        procedure delete_server_client_socket(p_socket_handle_string: String);
                                        function f_c_find_server_client_socket_index(p_socket_handle_string: String): Integer;
                                        function f_c_find_server_client_socket(p_socket_handle_string: String): c_server_client_socket;

                                        Destructor Destroy; Override;
                                      end; // c_server_client_socket_list

         // -- notify the reception of a client connection request
         t_po_accept_socket_event= procedure(p_c_server_socket: c_server_socket;
             p_c_server_client_socket: c_server_client_socket) of object;

         c_server_socket= class(c_base_socket)
                            m_c_parent_ref: c_basic_object;

                            // -- server has received client connection request
                            // --   and has called Accept to fork the server_client_socket
                            m_on_after_accept: t_po_accept_socket_event;

                            m_c_server_client_socket_list: c_server_client_socket_list;
                            m_c_class_of_server_client_socket: c_class_of_server_client_socket;

                            Constructor create_server_socket(p_name: String;
                                p_c_class_of_server_client_socket: c_class_of_server_client_socket);

                            procedure wsa_select; Virtual;

                            procedure do_bind_win_socket(p_port: Integer);
                            procedure do_listen_to_client(p_listen_queue_size: Integer);

                            procedure handle_wm_async_select(var pv_wm_async_select:
                                t_wm_async_select); Message wm_asynch_select;

                            procedure do_close_win_socket; Override;

                            Destructor Destroy; Override;
                          end; // c_server_socket

  implementation
    uses // SysUtils
       Forms // Application

      , u_c_display, u_display_hex_2, u_strings
      , u_winsock
      ;

    // -- c_server_client_socket

    Constructor c_server_client_socket.create_server_client_socket(p_name: String;
        p_c_server_socket_ref: c_server_socket); 
      begin
        Inherited create_base_socket_with_buffer(p_name);
        m_c_server_socket_ref:= p_c_server_socket_ref;
      end; // create_server_client_socket

    // -- empty abstract methods

    procedure c_server_client_socket.handle_can_write_data;
      begin
      end; // handle_can_write_data

    procedure c_server_client_socket.handle_received_data;
      begin
      end; // handle_received_data

    procedure c_server_client_socket.handle_remote_client_closed;
      begin
      end; // handle_remote_client_closed

    function c_server_client_socket.f_display_server_client_socket: String;
      begin
        Result:= f_display_base_socket;
      end; // f_display_server_client_socket

    // -- c_server_client_socket_list

    constructor c_server_client_socket_list.create_server_client_socket_list(p_name: String);
      begin
        Inherited create_basic_object(p_name);
        m_c_server_client_socket_list:= tStringList.Create;
      end; // create_server_client_socket_list

    function c_server_client_socket_list.f_c_server_client_socket(p_index: Integer): c_server_client_socket;
      begin
        Result:= c_server_client_socket(m_c_server_client_socket_list.Objects[p_index]);
      end; // f_c_server_client_socket

    procedure c_server_client_socket_list.display_server_client_socket_list;
      var l_client_index: Integer;
      begin
        for l_client_index:= 0 to m_c_server_client_socket_list.Count- 1 do
          display(f_c_server_client_socket(l_client_index).f_display_server_client_socket);
      end; // display_server_client_socket_list

    procedure c_server_client_socket_list.add_server_client_socket(p_socket_handle_string: String;
        p_c_server_client_socket: c_server_client_socket);
      begin
        m_c_server_client_socket_list.AddObject(p_socket_handle_string, p_c_server_client_socket);
      end; // add_server_client_socket

    procedure c_server_client_socket_list.delete_server_client_socket(p_socket_handle_string: String);
      var l_server_client_socket_index: Integer;
      begin
        l_server_client_socket_index:= f_c_find_server_client_socket_index(p_socket_handle_string);
        if l_server_client_socket_index>= 0
          then begin
              display('free '+ p_socket_handle_string);
              // -- close and free
              f_c_server_client_socket(l_server_client_socket_index).Free;
              // m_c_server_client_socket_list.Objects[l_server_client_socket_index]:= Nil;
              
              // -- remove from list
              m_c_server_client_socket_list.Delete(l_server_client_socket_index);
            end;
      end; // delete_server_client_socket

    function c_server_client_socket_list.f_c_find_server_client_socket_index(p_socket_handle_string: String): Integer;
      begin
        Result:= m_c_server_client_socket_list.IndexOf(p_socket_handle_string);
      end; // f_c_find_server_client_socket_index

    function c_server_client_socket_list.f_c_find_server_client_socket(p_socket_handle_string: String): c_server_client_socket;
      var l_index_of: Integer;
      begin
        l_index_of:= f_c_find_server_client_socket_index(p_socket_handle_string);
        // -- the key MUST be unique
        if l_index_of< 0
          then Result:= Nil
          else Result:= f_c_server_client_socket(l_index_of);
      end; // f_c_find_server_client_socket

    Destructor c_server_client_socket_list.Destroy;
      var l_client_index: Integer;
      begin
        display('> c_server_client_socket_list.Destroy');

        // -- close and free each server_client_socket
        for l_client_index:= 0 to m_c_server_client_socket_list.Count- 1 do
          f_c_server_client_socket(l_client_index).Free;
        
        m_c_server_client_socket_list.Free;

        Inherited;
        display('< c_server_client_socket_list.Destroy');
      end; // Destroy

    // -- c_server_socket

    Constructor c_server_socket.create_server_socket(p_name: String;
        p_c_class_of_server_client_socket: c_class_of_server_client_socket);
      begin
        Inherited create_base_socket(p_name);

        m_c_server_client_socket_list:= c_server_client_socket_list.create_server_client_socket_list('server_list');
        m_c_class_of_server_client_socket:= p_c_class_of_server_client_socket;
      end; // create_server_socket

    procedure c_server_socket.wsa_select;
      var l_result: Integer;
      begin
        trace_socket('');
        trace_socket('> wsa_select '+ f_name_and_handle(m_base_socket_handle));

        allocate_window;

        // -- specifies which WinSocket events should be monitored
        // -- receives
        // --   - fd_accept for c_server_socket
        // --   - fd_write, fd_read, fd_close for c_server_client_sockets
        trace_socket(':wsaAsyncSelect('+ f_socket_handle_string(m_base_socket_handle)+
            ', READ+ ACCEPT+ WRITE+ CLOSE, )');
        l_result:= wsaAsyncSelect(m_base_socket_handle, m_window_handle,
            wm_asynch_select,
            FD_READ+ FD_ACCEPT+ FD_WRITE+ FD_CLOSE);
        if l_result<> 0
          then raise_exception(e_wsa_select_error, WSAGetLastError, 'wsaAsyncSelect');

        trace_socket('< wsa_select');
      end; // wsa_select

    procedure c_server_socket.do_bind_win_socket(p_port: Integer);
      var l_result: Integer;
          l_address_socket_in: tSockAddrIn;
      begin
        trace_socket('');
        trace_socket('> do_bind_win_socket '+ f_name_and_handle(m_base_socket_handle));

        FillChar(l_address_socket_in, sizeof(l_address_socket_in), 0);
        with l_address_socket_in do
        begin
          sin_family:= af_Inet;
          sin_port:= hToNs(p_port);
          sin_addr.s_addr:= InAddr_Any; // $00000000
        end;

        trace_socket(':Bind('+ f_socket_handle_string(m_base_socket_handle)
            + ', '+ IntToStr(p_port)+ ', )');
        l_result:= Bind(m_base_socket_handle, l_address_socket_in,
            sizeof(l_address_socket_in));
        if l_result<> 0
          then display_socket_error_halt(m_base_socket_handle, 's.Bind',
              WSAGetLastError);

        trace_socket('< do_bind_win_socket '+ f_name_and_handle(m_base_socket_handle));
      end; // do_bind_win_socket

    procedure c_server_socket.do_listen_to_client(p_listen_queue_size: Integer);
        // -- used in server sockets
      var l_result: Integer;
      begin
        trace_socket('');
        trace_socket('> do_listen_to_client '+ f_name_and_handle(m_base_socket_handle));

        trace_socket(':Listen('+ f_socket_handle_string(m_base_socket_handle)+ ', )');
        l_result:= Listen(m_base_socket_handle, p_listen_queue_size);
        if l_result<> 0
          then display_socket_error_halt(m_base_socket_handle, 's.Listen',
              WSAGetLastError);

        trace_socket('< do_listen_to_client');
      end; // do_listen_to_client

    // -- Windows notifies our server socket

    procedure c_server_socket.handle_wm_async_select(var pv_wm_async_select: t_wm_async_select);
      // -- wParam: hSocket, lo(lParam): notification, hi(lParam): error
      // -- receives
      // --  - fd_Accept for the c_server_socket
      // --  - fd_Read, fd_Write, fd_Close for all the c_server_client_sockets

      procedure handle_fd_accept_notification(p_socket_handle: tSocket);
          // -- A Client was connected.
          // -- the Server spans a socket by calling Accept
        var l_address_socket_in: tSockAddrIn;
            l_address_size: Integer;
            l_server_client_socket_hande: tSocket;
            l_c_server_client_socket: c_server_client_socket;
            l_server_client_socket_hande_string: String;
        begin
          trace_socket('> fd_accept('+ f_name_and_handle(p_socket_handle)+ ')');

          // -- the socket handle received by fd_accept
          trace_socket(':Accept('+ IntToStr(p_socket_handle)+ ',...)');

          // -- Accept requires a variable
          l_address_size:= sizeof(l_address_socket_in);
          // -- this clones the server socket, (same parameters), and returns
          // --   the socket_handle of this clone
          // -- second parameter: will be filled with the caller (could decide to reject)
          l_server_client_socket_hande:= Accept(p_socket_handle, @l_address_socket_in, @l_address_size);

          // -- create this client socket, using a Class reference and a Virtual constructor
          l_c_server_client_socket:= m_c_class_of_server_client_socket.create_server_client_socket('serv_cli', Self);
          with l_c_server_client_socket do
          begin
            m_base_socket_handle:= l_server_client_socket_hande;
            // -- for debug, since all notifications will happen here
            m_window_handle:= Self.m_window_handle;
            // -- also the IPs and ports (for debug display)
            m_trace_socket:= Self.m_trace_socket;

            // -- ?? link the errors
          end; // with l_c_server_client_socket

          // -- the key in the list
          l_server_client_socket_hande_string:= IntToStr(l_server_client_socket_hande);
          // -- add the socket to the list
          m_c_server_client_socket_list.add_server_client_socket(l_server_client_socket_hande_string,
              l_c_server_client_socket);

          trace_socket('created_server_client_socket ='+ f_socket_name(l_server_client_socket_hande));

          // -- notify the user
          if Assigned(m_on_after_accept)
            then m_on_after_accept(Self, l_c_server_client_socket);

          trace_socket('< fd_accept');
        end; // handle_fd_accept_notification

      procedure handle_fd_write_notification(p_socket_handle: tSocket);
        var l_c_server_client_socket: c_server_client_socket;
        begin
          trace_socket('> fd_write('+ f_name_and_handle(p_socket_handle)+ ')');

          if p_socket_handle= m_base_socket_handle
            then begin
                trace_socket('*** fd_write: server_socket_handle'
                    + ' p_sock ='+ IntToStr(p_socket_handle)+ ' serv= '+ IntToStr(m_base_socket_handle));
              end
            else begin
                l_c_server_client_socket:= m_c_server_client_socket_list.f_c_find_server_client_socket(IntToStr(p_socket_handle));
                if l_c_server_client_socket<> Nil
                  then begin
                      l_c_server_client_socket.handle_can_write_data;
                    end
                  else trace_socket('*** fd_write: no_client_server_socket_handle (ok)'
                    + ' p_sock ='+ IntToStr(p_socket_handle));
              end;

          trace_socket('< fd_write');
        end; // handle_fd_write_notification

      procedure handle_fd_read_notification(p_socket_handle: tSocket);
        var l_c_server_client_socket: c_server_client_socket;
        begin
          trace_socket('> fd_read('+ f_name_and_handle(p_socket_handle)+ ')');

          if p_socket_handle= m_base_socket_handle
            then begin
                // -- server should not receive much
                trace_socket('*** server_handle_read');
              end
            else begin
                trace_socket('server_client_handle_read');
                // -- one of the c_server_client_sockets
                // -- find this socket
                l_c_server_client_socket:= m_c_server_client_socket_list.f_c_find_server_client_socket(IntToStr(p_socket_handle));

                if l_c_server_client_socket<> Nil
                  then begin
                      // -- let the user of the c_server_socket handle the reception and analysis
                      l_c_server_client_socket.handle_received_data;
                    end
                  else trace_socket('*** fd_write: no_clientt_server_socket_handle (ok)'
                    + ' p_sock ='+ IntToStr(p_socket_handle));
              end;

          trace_socket('< fd_read');
        end; // handle_fd_read_notification

      procedure handle_fd_close_notification(p_socket_handle: tSocket);
        var l_c_server_client_socket: c_server_client_socket;
        begin
          trace_socket('> fd_close('+ f_name_and_handle(p_socket_handle)+ '). The remote client closed');

          if p_socket_handle= m_base_socket_handle
            then begin
                // -- should not happen
                display(' *** server_close '+ f_name_and_handle(p_socket_handle));
              end
            else begin
                l_c_server_client_socket:= m_c_server_client_socket_list.f_c_find_server_client_socket(IntToStr(p_socket_handle));
                if l_c_server_client_socket<> Nil
                  then begin
                      l_c_server_client_socket.handle_remote_client_closed;
                      // -- close, free, delete from list
                      m_c_server_client_socket_list.delete_server_client_socket(IntToStr(p_socket_handle));
                    end
                  else display('  *** no_server_client_socket');
              end;

          trace_socket('< fd_close')
        end; // handle_fd_close_notification

      var l_socket_handle: tSocket;
          l_error, l_notification: Integer;

      begin // handle_wm_async_select
        l_socket_handle:= pv_wm_async_select.m_socket_handle;
        l_error:= pv_wm_async_select.m_select_error;
        l_notification:= pv_wm_async_select.m_select_event;

        trace_socket('');
        trace_socket('> handle_wm_async_select '+ m_name+ ' wnd_hnd=$'+ IntToHex(m_window_handle, 2)
            + ' err=$'+ IntToHex(l_error, 2)
            + ' notif='+ IntToHex(l_notification, 2)
            + ' sck_hnd='+ IntToStr(l_socket_handle));

        if l_error= 0
          then trace_socket('notif: '+ f_socket_notification_name(l_notification))
          else trace_socket('notif: '+ f_socket_notification_name(l_notification)
              + ' err '+ IntToStr(l_error));

        if l_error<= wsaBaseErr
          then begin
              case l_notification of
                FD_CONNECT: display_bug_halt('no_connect_in_server');
                FD_ACCEPT: handle_fd_accept_notification(l_socket_handle);
                FD_WRITE: // -- sent when can write in the buffer (after connect or when buffer is empty)
                          handle_fd_write_notification(l_socket_handle);
                FD_READ: // -- sent when received some data (reception buffer not empty)
                         handle_fd_read_notification(l_socket_handle);

                FD_CLOSE: handle_fd_close_notification(l_socket_handle);
                else trace_socket('unhandled_notification ');
              end // case
            end
          else begin
              if l_notification= FD_CLOSE
                then handle_fd_close_notification(l_socket_handle)
                else raise_exception(e_notification_error, l_error, 'wm_asynch');
            end;

        trace_socket('< handle_wm_async_select');
      end; // handle_wm_async_select

    procedure c_server_socket.do_close_win_socket;
      begin
        trace_socket('');
        trace_socket('> do_close_socket '+ f_name_and_handle(m_base_socket_handle));

        // -- force server_client_socket close
        m_c_server_client_socket_list.Free;
        m_c_server_client_socket_list:= c_server_client_socket_list.create_server_client_socket_list('server_list');

        Inherited;
        trace_socket('< do_close_socket');
      end; // do_close_win_socket

    Destructor c_server_socket.Destroy;
      begin
        trace_socket('');
        trace_socket('> c_server_socket.Destroy');

        if m_is_open and (m_base_socket_handle<> INVALID_SOCKET)
          then do_close_win_socket;

        m_c_server_client_socket_list.Free;

        if m_window_handle<> 0
          then DeallocateHWnd(m_window_handle);

        // -- should call WSACleanup

        Inherited;

        trace_socket('< c_server_socket.Destroy');
      end; // Destroy

    begin // u_c_server_socket
    end. // u_c_server_socket


