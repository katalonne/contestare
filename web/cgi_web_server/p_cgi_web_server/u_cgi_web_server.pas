unit u_cgi_web_server;
  interface
    uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs
        , StdCtrls
        , ExtCtrls, ComCtrls
        , u_c_base_socket_4

        , u_c_cgi_web_server
        , u_c_cgi_http_server_client_socket
        ;

    type TForm1= class(TForm)
    Panel1: TPanel;
    edit_port: TEdit;
    clear_: TButton;
    close_: TPanel;
    exit_: TButton;
    open_: TPanel;
    ListBox1: TListBox;
    PageControl1: TPageControl;
    display_: TTabSheet;
    traffic_: TTabSheet;
    Memo1: TMemo;
    Memo2: TMemo;
    show_: TCheckBox;
                   procedure FormCreate(Sender: TObject);
    procedure close_Click(Sender: TObject);
    procedure clear_Click(Sender: TObject);
    procedure exit_Click(Sender: TObject);
    procedure open_Click(Sender: TObject);
                   private
                     procedure handle_cgi_server_accept(p_c_cgi_web_server: c_cgi_web_server;
                         p_c_cgi_http_server_client_socket: c_cgi_http_server_client_socket);
                     procedure handle_closed_server_win_socket(p_c_cgi_http_server_client_socket: c_cgi_http_server_client_socket);
                     procedure handle_display_traffic(p_traffic: String);
                   public
                 end;

    var Form1: TForm1;

  implementation
    uses
        u_c_log, u_c_display, u_display_hex_2, u_types_constants
        , u_file
        , u_winsock
      ;

    {$R *.DFM}

 
    const // k_site_path= '..\..\cgi_web_server\_site\';
          k_site_path= '..\_site\';
          k_cgi_exe_path= k_site_path;

    var g_c_cgi_web_server: c_cgi_web_server= Nil;

    procedure set_panel_color(p_c_panel: tPanel; p_color: Integer);
      begin
        p_c_panel.Color:= p_color;
        Application.ProcessMessages;
      end; // set_panel_color

    // -- user actions

    procedure TForm1.FormCreate(Sender: TObject);
      begin
        initialize_display(Memo1.Lines);
        initialize_default_log;

        set_panel_color(open_, clLime);
        set_panel_color(close_, clRed);
      end; // FormCreate

    procedure TForm1.clear_Click(Sender: TObject);
      begin
        clear_display;
      end; // clear_Click

    procedure TForm1.close_Click(Sender: TObject);
      begin
        g_c_cgi_web_server.Free;
        g_c_cgi_web_server:= Nil;
      end; // CloseSocket_Click

    procedure TForm1.open_Click(Sender: TObject);
      begin
        g_c_cgi_web_server:= c_cgi_web_server.create_cgi_web_server('web_server', k_site_path, k_cgi_exe_path);
        with g_c_cgi_web_server do
        begin
          m_trace_web_server:= True;

          m_on_cgi_server_accept:= handle_cgi_server_accept;
          start_web_server(StrToInt(edit_port.Text));
        end;
        set_panel_color(open_, clRed);
        set_panel_color(close_, clLime);
      end; // open_Click

    procedure TForm1.handle_cgi_server_accept(p_c_cgi_web_server: c_cgi_web_server;
        p_c_cgi_http_server_client_socket: c_cgi_http_server_client_socket);
      begin
        display('Accept');

        ListBox1.Items.Add(f_socket_handle_string(p_c_cgi_http_server_client_socket.m_base_socket_handle));
        p_c_cgi_http_server_client_socket.m_on_cgi_http_server_client_socket_closed:= handle_closed_server_win_socket;
        p_c_cgi_http_server_client_socket.m_on_display_traffic:= handle_display_traffic;
        p_c_cgi_http_server_client_socket.m_show_console:= show_.Checked;
      end; // handle_cgi_server_accept

    procedure TForm1.handle_display_traffic(p_traffic: String);
      begin
        Memo2.Lines.Add(p_traffic);
      end; // handle_display_traffic

    procedure TForm1.handle_closed_server_win_socket(p_c_cgi_http_server_client_socket: c_cgi_http_server_client_socket);
      var l_handle_string: String;
      begin
        l_handle_string:= f_socket_handle_string(p_c_cgi_http_server_client_socket.m_save_socket_handle);
        display('closed '+ l_handle_string);
        with ListBox1.Items do
          if IndexOf(l_handle_string)>= 0
            then Delete(IndexOf(l_handle_string));
      end; // handle_closed_server_win_socket

    procedure TForm1.exit_Click(Sender: TObject);
      begin
        g_c_cgi_web_server.Free;
        g_c_display.Free;
        Close;
      end; // exit_Click

    end.
