// 004 u_c_http_server_client_socket
// 20 may 2005

// -- (C) Felix John COLIBRI 2004
// -- documentation: http://www.felix-colibri.com

(*$r+*)

unit u_c_http_server_client_socket;
  interface
    uses u_c_basic_object
        , u_c_server_socket_4
        ;

    type t_http_answer_error= (e_no_http_answer_eror,
               e_no_GET, e_post_no_exe);
    const // -- if not page in the GET, use this page
          k_default_HTML_page= 'index.html';

    type t_mime_name= record
                        m_mime_extension: string;
                        m_mime_type: string;
                      end;
    const k_mime_names: array[0..6] of t_mime_name=
            ((m_mime_extension: 'gif'; m_mime_type: 'image/gif'),
             (m_mime_extension: 'jpg'; m_mime_type: 'image/jpg'),
             (m_mime_extension: 'htm'; m_mime_type: 'text/html'),
             (m_mime_extension: 'html'; m_mime_type: 'text/html'),
             (m_mime_extension: 'css'; m_mime_type: 'text/css'),
             (m_mime_extension: 'js'; m_mime_type: 'text/javascript'),
             (m_mime_extension: 'txt'; m_mime_type: 'text/plain'));
           k_default_MIME_type= 'text/html';

           k_answer_server= 'Server: my_server'#13#10;
           k_answer_http_200_ok= 'HTTP/1.1 200 OK'#13#10;
           k_format_message_404= '<HEAD><TITLE>404 Page not found</TITLE></HEAD>'#13#10+
              '<BODY><H1>Error 404 Page not found</H1>'#13#10+
              'The requested URL <%s> is not on this server.<BR>'#13#10+
              '</BODY>'#13#10#13#10;

    type c_http_server_client_socket= class(c_server_client_socket)
                                        m_site_path: String;
                                        
                                        m_http_answer_error: t_http_answer_error;
                                        m_page_name: String;

                                        Constructor create_server_client_socket(p_name: String;
                                            p_c_server_socket_ref: c_server_socket); Override;

                                        procedure handle_can_write_data; Override;
                                        procedure handle_received_data; Override;
                                        procedure handle_remote_client_closed; Override;

                                          function f_mime_type(p_page_name: String): String;
                                          function f_answer_content_type(p_MIME_type: String): String;
                                          function f_answer_content_length(p_body_length: Integer): String;
                                          function f_server_full_file_name(p_file_name: string): String;
                                          procedure send_page_not_found(p_page_name: String);
                                        procedure read_file_and_send_page(p_page_name: String);

                                        Destructor Destroy; Override;
                                      end; // c_http_server_client_socket

  implementation
    uses Windows, Classes, SysUtils
        , u_c_display, u_types_constants, u_characters, u_strings
        , u_file
        ;

    // -- c_http_server_client_socket

    Constructor c_http_server_client_socket.create_server_client_socket(p_name: String;
        p_c_server_socket_ref: c_server_socket);
      begin
        Inherited create_server_client_socket(p_name, p_c_server_socket_ref);
      end; // create_server_client_socke

    procedure c_http_server_client_socket.handle_can_write_data;
      begin
        trace_socket('> c_http_client.write');
        trace_socket('< c_http_client.write');
      end; // handle_can_write_data

    procedure c_http_server_client_socket.handle_received_data;
        // -- take the header appart
        // -- should at least retrieve
        // --  - the method GET
        // --  - the page path and file name
      var l_end_of_header_position: Integer;
          l_read_index: Integer;
      begin
        trace_socket('> c_http_client.read');

        // -- fetch the bytes
        receive_buffered_data;

        with m_c_reception_buffer do
        begin
          display('> analyze_header');

          // -- presently only analyze GET and the name
          l_read_index:= f_string_position(m_read_index, 'get');

          display('get_at '+ IntToStr(l_read_index));
          // display(f_extract_string_start_end(0, m_write_index- 1));

          if l_read_index< 0
            then begin
                m_page_name:= '';
                m_http_answer_error:= e_no_GET;
              end
            else begin
                // -- skip GET
                m_read_index:= l_read_index+ 3;
                // -- skip blanks
                skip_blanks(m_read_index);
                m_page_name:= f_extract_non_blank(m_read_index);
                Inc(m_read_index, Length(m_page_name));
                
                if (m_page_name= '/') or (m_page_name= '')
                  then m_page_name:= k_default_HTML_page;

                read_file_and_send_page(m_page_name);
              end;
          display('< analyze_header');
        end; // with p_c_byte_buffer

        trace_socket('< c_http_client.read');
      end; // handle_received_data

    procedure c_http_server_client_socket.handle_remote_client_closed;
      begin
      end; // handle_remote_client_closed

    // -- handling of the http request

    function c_http_server_client_socket.f_mime_type(p_page_name: String): String;
        // -- extract the extension for the MIME type
        // --   can be part of the default page
      var l_dot_position: Integer;
          l_extension: String;
          l_MIME_index: Integer;
      begin
        l_dot_position:= Pos('.', p_page_name);

        if l_dot_position<= 0
          then begin
              display('default extension: txt');
              l_extension:= 'txt';
            end
          else begin
              // display('read '+ IntToStr(m_read_index)+ ' dot '+ IntToStr(l_dot_position));
              l_extension:= Copy(p_page_name, l_dot_position+ 1, Length(p_page_name)+ 1- (l_dot_position+ 1));
              display('ext >'+ l_extension+ '<');
            end;

        Result:= 'CONTENT ?';
        for l_MIME_index:= LOW(k_mime_names) to HIGH(k_mime_names) do
          if k_mime_names[l_MIME_index].m_mime_extension= l_extension
            then
              begin
                Result:= k_mime_names[l_MIME_index].m_mime_type;
                break;
              end;
     end; // f_mime_type

    function c_http_server_client_socket.f_answer_content_type(p_MIME_type: String): String;
      begin
        Result:= 'Content-Type: '+ p_MIME_type+ #13#10;
      end; // f_answer_content_type

    function c_http_server_client_socket.f_answer_content_length(p_body_length: Integer): String;
      begin
        Result:= 'Content-Length: '+ IntToStr(p_body_length)+ #13#10;
      end; // f_answer_content_length

    function c_http_server_client_socket.f_server_full_file_name(p_file_name: string): String;
        // -- "/index.html"  or with other segments
        // --  - remove starting /
        // --  - convert other / to \
      // var l_site_path: String;
      begin
        Result:= p_file_name;

        if Length(Result)> 0
          then begin
              if Result[1]= '/'
                then Delete(Result, 1, 1);
              Result:= f_replace_character(Result, '/', '\');

              // l_site_path:= (m_c_server_socket_ref.m_c_parent_ref as c_web_server).m_site_path;
              Result:= m_site_path+ Result;
              // check_path_and_name(Result);
            end;
      end; // f_server_file_name

    procedure c_http_server_client_socket.read_file_and_send_page(p_page_name: String);
      var l_file_name: String;
          l_file: File;
          l_file_size: Integer;
          l_pt_buffer: t_pt_bytes;
          l_MIME_type: String;
          l_header: String;
      begin
        display('from_browser '+ p_page_name);
        l_file_name:= f_server_full_file_name(p_page_name);
        display('search '+ l_file_name);
        check_path_and_name(l_file_name);

        if FileExists(l_file_name)
          then begin
              AssignFile(l_file, l_file_name);
              Reset(l_file, 1);
              l_file_size:= FileSize(l_file);
              GetMem(l_pt_buffer, l_file_size);
              BlockRead(l_file, l_pt_buffer^, l_file_size);

              l_MIME_type:= f_mime_type(m_page_name);

              l_header:=
                    k_answer_http_200_ok
                  + k_answer_server
                  // + 'Server: Microsoft-IIS/5.0'+ k_new_line
                  // + 'MIME-version: 1.0'#13#10
                  + f_answer_content_type(l_MIME_type)
                  + 'Accept-Ranges: bytes'+ k_new_line
                  + f_answer_content_length(l_file_size)
                  + k_new_line;

              f_do_send_string(l_header);
              f_do_send_buffer(l_pt_buffer, l_file_size);

              FreeMem(l_pt_buffer, l_file_size);
            end
          else send_page_not_found(p_page_name);
      end; // read_file_and_send_page

    procedure c_http_server_client_socket.send_page_not_found(p_page_name: String);
      var l_header, l_body: String;
      begin
        display('did_not_find >'+ p_page_name+ '<');

        l_header:=
              k_answer_http_200_ok
            + k_answer_server
            + f_answer_content_type(k_default_MIME_type)
            + k_new_line;

        l_body:= Format(k_format_message_404, [p_page_name]);

        f_do_send_string(l_header+ l_body);
      end; // send_page_not_found

    Destructor c_http_server_client_socket.Destroy;
      begin
        Inherited;
      end; // Destroy

    begin // u_c_http_server_client_socket
    end. // u_c_http_server_client_socket
