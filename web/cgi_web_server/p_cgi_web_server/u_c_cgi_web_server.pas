unit u_c_cgi_web_server;
  interface
    uses u_c_basic_object
        , u_c_byte_buffer
        , u_c_base_socket_4
        , u_c_server_socket_4
        , u_c_cgi_http_server_client_socket
        ;

    type c_cgi_web_server= Class;
         t_po_web_server_event= Procedure(p_c_cgi_web_server: c_cgi_web_server;
             p_c_cgi_http_server_client_socket: c_cgi_http_server_client_socket) of object;

         c_cgi_web_server= class(c_basic_object)
                             m_c_server_socket: c_server_socket;

                             m_trace_web_server: Boolean;
                             m_site_path, m_cgi_exe_path: String;
                             m_on_cgi_server_accept: t_po_web_server_event;

                             Constructor create_cgi_web_server(p_name,
                                 p_site_path, p_cgi_exe_path: String);

                             procedure start_web_server(p_port: Integer);
                             procedure handle_accept(p_c_server_socket: c_server_socket;
                                 p_c_server_client_socket: c_server_client_socket);
                             procedure handle_closed_server_win_socket(p_c_base_socket: c_base_socket);

                             procedure close_web_server;

                             Destructor Destroy; Override;
                           end; // c_cgi_web_server

  implementation
    uses Classes, SysUtils, u_c_display, u_types_constants
        , u_c_http_server_client_socket
        , u_winsock
        ;

    // -- c_cgi_web_server

    Constructor c_cgi_web_server.create_cgi_web_server(p_name,
        p_site_path, p_cgi_exe_path: String);
      begin
        Inherited create_basic_object(p_name);
        m_site_path:= p_site_path;
        m_cgi_exe_path:= p_cgi_exe_path;

        // -- specify which c_server_client_sockt Class should be created
        m_c_server_socket:= c_server_socket.create_server_socket('server',
            c_cgi_http_server_client_socket);
        m_c_server_socket.m_on_after_accept:= handle_accept;
      end; // create_cgi_web_server

    procedure c_cgi_web_server.start_web_server(p_port: Integer);
      begin
        with m_c_server_socket do
        begin
          m_trace_socket:= m_trace_web_server;

          wsa_startup;
          create_win_socket;
          wsa_select;
          do_bind_win_socket(p_port);
          do_listen_to_client(k_default_listen_queue_size)
        end; // with m_c_server_socket
      end; // start_web_server

    procedure c_cgi_web_server.handle_accept(p_c_server_socket: c_server_socket;
        p_c_server_client_socket: c_server_client_socket);
      begin
        (p_c_server_client_socket as c_http_server_client_socket).m_site_path:= m_site_path;
        (p_c_server_client_socket as c_cgi_http_server_client_socket).m_cgi_exe_path:= m_cgi_exe_path;

        p_c_server_client_socket.m_on_after_do_close_socket:= handle_closed_server_win_socket;

        if Assigned(m_on_cgi_server_accept)
          then m_on_cgi_server_accept(Self, p_c_server_client_socket as c_cgi_http_server_client_socket)
      end; // handle_accept

    procedure c_cgi_web_server.handle_closed_server_win_socket(p_c_base_socket: c_base_socket);
      begin
        display('> c_cgi_web_server.server_client_closed');
        m_c_server_socket.m_c_server_client_socket_list.delete_server_client_socket(IntToStr(p_c_base_socket.m_save_socket_handle));
        display('< c_cgi_web_server.server_client_closed');
      end; // handle_closed_server_win_socket

    procedure c_cgi_web_server.close_web_server;
      begin
        m_c_server_socket.do_close_win_socket;
      end; // close_web_server

    Destructor c_cgi_web_server.Destroy;
      begin
        close_web_server;
        m_c_server_socket.Free;

        Inherited;
      end; // Destroy

    begin // u_c_cgi_web_server
    end. // u_c_cgi_web_server
